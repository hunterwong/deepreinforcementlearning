# win_model.py

import ast

import torch
import torch.nn as nn
import torch.utils.data
import numpy as np
import pandas as pd


class model(nn.Module):
    """Predicts value from a given state."""
    def __init__(self):
        super(model, self).__init__()
        input_shape = (2, 8, 8)
        NUM_FILTERS = 32

        self.conv1 = nn.Sequential(
            nn.Conv2d(input_shape[0], NUM_FILTERS, kernel_size=3),
            nn.BatchNorm2d(NUM_FILTERS),
            nn.ReLU()
        )
        self.conv2 = nn.Sequential(
            nn.Conv2d(NUM_FILTERS, NUM_FILTERS, kernel_size=3),
            nn.BatchNorm2d(NUM_FILTERS),
            nn.ReLU()
        )
        self.lin1 = nn.Sequential(
            nn.Linear(NUM_FILTERS*4*4, 128),
            nn.Dropout(.2),
            nn.ReLU()
        )
        self.lin2 = nn.Sequential(
            nn.Linear(128, 64),
            nn.Dropout(.2),
            nn.ReLU()
        )
        self.out = nn.Sequential(
            nn.Linear(64, 1),
            nn.Tanh()
        )

    def forward(self, x):
        x = self.conv1(x)
        x = self.conv2(x)
        #x = torch.reshape(x, (-1,))
        x = torch.flatten(x)
        x = self.lin1(x)
        x = self.lin2(x)
        x = self.out(x)
        return x


def load_data():
    data_path = "./data/final_states_train.csv"
    data = pd.read_csv(data_path)
    y = data.Winner[1:]
    states = data.State[1:]
    # convert states into a list of torch tensors
    x = [torch.tensor(ast.literal_eval(state), dtype=torch.int8) for state in states]
    #print(type(x), type(x[0]), type(x[0][0]), type(x[0][0][0]), type(x[0][0][0][0]), type(x[0][0][0][0].item()))
    #print(x)
    #data = pd.DataFrame({'State': x, "Winner": y})
    return x, y

def train(x, y):
    #dataframe = dataframe.sample(frac=1).reset_index(drop=True)  # shuffle
    #x = dataframe["State"].values
    #y = dataframe["Winner"].values

    device = torch.device("cuda:0")
    print(device)

    net = model()
    criterion = nn.MSELoss()
    optimizer = torch.optim.Adam(net.parameters(), lr=0.001)

    for epoch in range(2):  # loop over the dataset multiple times
        running_loss = 0.0
        i = 0
        for inputs, labels in zip(x,y):
            inputs = inputs.unsqueeze(0)
            inputs = inputs.type('torch.FloatTensor')
            labels = torch.tensor(labels, dtype=torch.float)
            # zero the parameter gradients
            optimizer.zero_grad()

            # forward + backward + optimize
            outputs = net(inputs)

            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()

            # print statistics
            running_loss += loss.item()
            if i % 500 == 499:    # print every 2000 mini-batches
                print('[%d, %5d] loss: %.3f' %
                      (epoch + 1, i + 1, running_loss / 500))
                running_loss = 0.0
            i += 1
    print('Finished Training')

if __name__ == "__main__":
    x, y = load_data()
    train(x, y)
