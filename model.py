# model.py


import torch
import torch.nn as nn
import numpy as np


class AlphaNet(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        input_shape = (2, 8, 8)
        NUM_FILTERS = 256
        # A peice can be anywhere on the board execpt for the opponents back row
        # all peices have 3 moves at most.
        actions_n = 8*7*3

        self.conv_in = nn.Sequential(
            nn.Conv2d(input_shape[0], NUM_FILTERS, kernel_size=3),
            nn.BatchNorm2d(NUM_FILTERS),
            nn.ReLU()
        )

        # layers with residual
        self.res = nn.Sequential(
            nn.Conv2d(NUM_FILTERS, NUM_FILTERS, kernel_size=3),
            nn.BatchNorm2d(NUM_FILTERS),
            nn.ReLU(),
            nn.Conv2d(NUM_FILTERS, NUM_FILTERS, kernel_size=3),
            nn.BatchNorm2d(NUM_FILTERS),
        )
        self.res_blocks = [self.res] * 19

        body_out_shape = (NUM_FILTERS, ) + input_shape[1:]

        # value head
        self.conv_val = nn.Sequential(
            nn.Conv2d(NUM_FILTERS, 1, kernel_size=1),
            nn.BatchNorm2d(1),
            nn.ReLU()
        )
        conv_val_size = self._get_conv_val_size(body_out_shape)
        self.value = nn.Sequential(
            nn.Linear(conv_val_size, 256),
            nn.ReLU(),
            nn.Linear(256, 1),
            nn.Tanh()
        )

        # policy head
        self.conv_policy = nn.Sequential(
            nn.Conv2d(NUM_FILTERS, 2, kernel_size=1),
            nn.BatchNorm2d(2),
            nn.ReLU()
        )
        conv_policy_size = self._get_conv_policy_size(body_out_shape)
        self.policy = nn.Sequential(
            nn.Linear(conv_policy_size, actions_n)
        )

    def _get_conv_val_size(self, shape):
        o = self.conv_val(torch.zeros(1, *shape))
        return int(np.prod(o.size()))

    def _get_conv_policy_size(self, shape):
        o = self.conv_policy(torch.zeros(1, *shape))
        return int(np.prod(o.size()))

    def forward(self, x):
        batch_size = x.size()[0]
        v = self.conv_in(x)
        for block in self.res_blocks:
            v = v + block(v)
        v = nn.functional.relu(v)
        val = self.conv_val(v)
        val = (val.view(batch_size, -1))
        pol = self.conv_policy(v)
        pol = self.policy(pol.view(batch_size, -1))
        return pol, val
