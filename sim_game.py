# random agent

import random
import csv
import torch
import numpy as np

import value_model
import breakthroughNN
from Games.breakthrough.breakthrough import model
from Games.breakthrough.breakthrough.model import Player


class RandAgent:

    def __init__(self, player, game):
        """player is either Player.BLACK or Player.WHITE."""
        self.color = player
        self.game = game

    def __init__(self, player):
        """player is either Player.BLACK or Player.WHITE."""
        self.color = player

    def _choose_random_piece_with_valid_move(self):
        """Returns the loc of a random piece of the correct color and
        it's valid moves
        """
        piece_locations = [loc for loc, player in self.game.board.items() if player == self.color]
        while True:  # search for a piece with a valid move
            chosen_piece_loc = random.choice(piece_locations)
            valid_moves = self.game.valid_moves(chosen_piece_loc)
            if valid_moves:
                return chosen_piece_loc, valid_moves

    def choose_move(self):
        """Given the fromloc, returns a random valid toloc."""
        chosen_peice, valid_moves = self._choose_random_piece_with_valid_move()
        chosen_move_loc = random.choice(valid_moves)
        return chosen_peice, chosen_move_loc

    def set_game(self, game):
        self.game = game

class NetworkAgent:
    def __init__(self, player, net):
        """player is either Player.BLACK or Player.WHITE."""
        self.color = player
        self.net = net

    def _gen_next_states(self):
        next_states = []  # [(board, move_from, move_to)]
        piece_locations = [loc for loc, player in self.game.board.items() if player == self.color]
        for piece in piece_locations:
            for loc in self.game.valid_moves(piece):
                new_board = self.game.result_of_move(piece, loc)
                next_states.append((new_board, piece, loc))
        return next_states

    def choose_move(self):
        next_states = self._gen_next_states()
        best_state_value = -2  # number below minimum value
        best_move = None  # the move that produces the best state value
        for state, piece, loc in next_states:
            state_tensor = self.game.state_as_tensor(state, self.game.player_in_turn)
            state_tensor = state_tensor.unsqueeze(0)
            #state_tenser = state.type('torch.FloatTensor')
            #print("getting value")
            value = self.net(state_tensor)[0].item()
            #print(value)
            #print(value > best_state_value)
            if value > best_state_value:
                best_state_value = value
                best_move = (piece, loc)
        return best_move[0], best_move[1]

    def set_game(self, game):
        self.game = game


class PolicyAgent:
    def __init__(self, player, net):
        """player is either Player.BLACK or Player.WHITE."""
        self.color = player
        self.net = net
        self.game = None
    def choose_move(self):
        state = self.game.state_as_tensor(self.game.board, self.game.player_in_turn)
        v, policy = self.net(state.unsqueeze(0))
        valids = self.game.all_valid_moves_as_flat_rep(state)
        policy = policy * valids
        policy = self._renormalize_policy(policy)
        if policy is None:
            print("All valid moves were masked, do workaround.")
            policy = valids / torch.sum(valids)
        mv = torch.argmax(policy)
        fromloc, toloc = self.game.movenum_to_move(mv)
        return fromloc, toloc

    def set_game(self, game):
        self.game = game

    def _renormalize_policy(self, policy):
        policy_sum = torch.sum(policy)
        if policy_sum > 0:
            #policy /= policy_sum
            return policy / policy_sum
        else:
            return None

class SimulateGame:
    def __init__(self, game, agent1, agent2):
        """Simulates a game between two agents.
        game is an instance of the breakthrough game
        agent1 is in white, agent2 is in black
        """
        self.game = game
        self.player_white = agent1
        self.player_black = agent2
        self.states = [(self.game.board.copy(), self.game.player_in_turn)]

    def run(self):
        while self.game.winner is None:
            if self.game.player_in_turn == Player.WHITE:
                piece_loc, chosen_move_loc = self.player_white.choose_move()
            elif self.game.player_in_turn == Player.BLACK:
                piece_loc, chosen_move_loc = self.player_black.choose_move()
            if chosen_move_loc in self.game.valid_moves(piece_loc):
                self.game.move(piece_loc, chosen_move_loc)
                self.states.append((self.game.board.copy(), self.game.player_in_turn))
            else:
                print("Agent chose an invalid move.")
        #self._print_states_as_tensors()
        print(self.game.winner)

    def _print_states_as_tensors(self):
        for state in self.states:
            print("\nNEXT TURN")
            print(self.game.state_as_tensor(state))


def create_final_state_data():
    win_table = {Player.WHITE: 1, Player.BLACK: 2}
    intermediate_states = []
    final_states = []
    for i in range(5000):
        game = model.Game()
        a1 = RandAgent(Player.WHITE, game)
        a2 = RandAgent(Player.BLACK, game)
        sim = SimulateGame(game, a1, a2)
        sim.run()
        for state, player in sim.states[:-1]:
            tensor = game.state_as_tensor(state, player)
            lst_of_state = tensor.tolist()
            intermediate_states.append(lst_of_state)
            if random.random() < .015:  # proportion of random turns to keep
                tensor = game.state_as_tensor(state, player)
                lst_of_state = tensor.tolist()
                intermediate_states.append(lst_of_state)
        tensor = game.state_as_tensor(sim.states[-1][0], sim.states[-1][1])
        lst_of_state = tensor.tolist()
        final_states.append((lst_of_state, win_table[game.winner]))

    with open('./data/final_states_train.csv', 'w') as csvfile:
        writer = csv.writer(csvfile, lineterminator='\n')
        writer.writerow(["Game_ID"] + ['State'] + ['Winner'], )
        i = 0
        for state in intermediate_states:
            writer.writerow([i] + [state] + ['0'])
            i += 1
        for state, winner in final_states:
            writer.writerow([i] + [state] + [winner])
            i += 1


def run_rand_games():
    a1 = RandAgent(Player.WHITE, game)
    a2 = RandAgent(Player.BLACK, game)
    run_games(a1, a2)


def run_value_games():
    net = value_model.model()
    net.load_state_dict(torch.load("./models/value_model1.pt"))
    a1 = NetworkAgent(Player.WHITE, net)
    a2 = NetworkAgent(Player.BLACK, net)
    run_games(a1, a2)

def run_policy_games():
    net = breakthroughNN.NN()
    a1 = PolicyAgent(Player.WHITE, net)
    a2 = PolicyAgent(Player.BLACK, net)
    run_games(a1, a2)


def run_games(agent1, agent2):
    device = torch.device("cuda:0")
    print(device)

    white_wins = 0
    black_wins = 0
    for i in range(100):
        game = model.Game()
        agent1.set_game(game)
        agent2.set_game(game)
        SimulateGame(game, agent1, agent2).run()
        if game.winner == Player.WHITE:
            white_wins += 1
        if game.winner == Player.BLACK:
            black_wins += 1
    print("White Wins:", white_wins)
    print("Black Wins:", black_wins)

if __name__ == "__main__":
    #create_final_state_data()
    #run_rand_games()
    #run_value_games()
    run_policy_games()
