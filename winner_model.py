# win_model.py

import ast
import random

import torch
import torch.nn as nn
import torch.utils.data
import numpy as np
import pandas as pd


class WinnerNet(nn.Module):
    """Given a state, predicts the winner in a state"""
    def __init__(self):
        super(WinnerNet, self).__init__()
        input_shape = (2, 8, 8)
        NUM_FILTERS = 64
        # A peice can be anywhere on the board execpt for the opponents back row
        # all peices have 3 moves at most.
        actions_n = 8*8*3

        self.conv_in = nn.Sequential(
            nn.Conv2d(input_shape[0], NUM_FILTERS, kernel_size=3),
            nn.BatchNorm2d(NUM_FILTERS),
            nn.ReLU()
        )

        # layers with residual
        self.res = nn.Sequential(
            nn.Conv2d(NUM_FILTERS, NUM_FILTERS, kernel_size=3),
            nn.BatchNorm2d(NUM_FILTERS),
            nn.ReLU(),
            nn.Conv2d(NUM_FILTERS, NUM_FILTERS, kernel_size=3),
            nn.BatchNorm2d(NUM_FILTERS),
        )
        self.out = nn.Linear(NUM_FILTERS*2*2, 3)

    def forward(self, x):
        x = self.conv_in(x)
        x = self.res(x)
        x = nn.functional.relu(x)
        x = x.flatten()
        x = self.out(x)
        x = nn.Softmax(dim=0)(x)
        return x


def load_data():
    data_path = "./data/final_states_train.csv"
    data = pd.read_csv(data_path)
    winners = data.Winner[1:]
    #y = []
    y = [torch.tensor([winner], dtype=torch.long) for winner in winners]
    states = data.State[1:]
    # convert states into a list of torch tensors
    x = [torch.tensor(ast.literal_eval(state), dtype=torch.int8) for state in states]
    #data = pd.DataFrame({'State': x, "Winner": y})
    data = list(zip(x,y))
    random.shuffle(data)
    return data

def train(data):
    device = torch.device("cuda:0")
    print(device)

    net = WinnerNet()
    criterion = nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(net.parameters(), lr=0.001)

    for epoch in range(8):  # loop over the dataset multiple times
        correct = 0
        incorrect = 0
        running_loss = 0.0
        i = 0
        for inputs, labels in data:
            inputs = inputs.unsqueeze(0)
            inputs = inputs.type('torch.FloatTensor')
            #labels = labels.unsqueeze(0)
            # zero the parameter gradients
            optimizer.zero_grad()
            # forward + backward + optimize
            outputs = net(inputs)
            outputs = outputs.unsqueeze(0)
            _, predicted = torch.max(outputs.data, 1)
            if predicted == labels:
                correct += 1
            else:
                incorrect += 1
            loss = criterion(outputs, labels)
            loss.backward()
            optimizer.step()
            # print statistics
            running_loss += loss.item()
            if i % 3000 == 2999:    # print every 2000 mini-batches
                print('[%d, %5d] loss: %.3f' %
                      (epoch + 1, i + 1, running_loss / 3000),
                      "Accuracy:", round(correct/i * 100, 5), "%",
                      "Incorrect:", incorrect)
                running_loss = 0.0
            i += 1
        # print at end of epoch
        print('[%d, %5d] loss: %.3f' %
              (epoch + 1, i + 1, running_loss / i),
              "Accuracy:", round(correct/i * 100, 5), "%",
              "Incorrect:", incorrect)
    print('Finished Training')
    torch.save(net, "./models/test_model.pt")
    print("Model saved!")

def test_model(data):
    device = torch.device("cuda:0")
    print(device)
    net = torch.load("./models/test_model.pt")
    total_params = sum(p.numel() for p in net.parameters() if p.requires_grad)
    print("Model has", total_params, "trainable parameters.")

    correct = 0
    total = 0
    with torch.no_grad():
        for state, labels in data:
            state = state.unsqueeze(0)
            state = state.type('torch.FloatTensor')
            outputs = net(state)
            outputs = outputs.unsqueeze(0)
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()
    print('Accuracy: %d %%' % (100 * correct / total))


if __name__ == "__main__":
    data = load_data()
    train(data)
    test_model(data)
