# trainer.py

import os
import random
import time

import torch
#from torch import nn
import numpy as np
from pickle import Pickler, Unpickler

from Games.breakthrough.breakthrough.model import Game, Player
from breakthrough_mcts2 import MCTS
from breakthroughNN import NN, ResNetwork
from playground import Playground


class Trainer:
    def __init__(self, game, net):
        self.game = game
        self.latest_net = net
        self.best_net = net.__class__()
        self.best_net.load_state_dict(net.state_dict()) # copy
        self.best_net.eval()
        self.mcts = MCTS(game, self.best_net)

        self.data_history = []
        self.n_new_nets = 0
        self._white_wins = 0
        self._black_wins = 0
        self.turns = 0

        # training parameters
        self.temp_threshold = 45
        #self.pol_criterion = torch.nn.CrossEntropyLoss()
        self.val_criterion = torch.nn.MSELoss()
        self.optimizer = torch.optim.Adam(self.latest_net.parameters(), lr=0.001)
        self.batch_size = 256
        self.iterations = 100
        self.n_games_to_sim = 250
        self.train_loops = 100
        self.eval_games = 40
        self.train_on_n_recent_games = 5000 * 50 # estimate 50 turns per game

    def train(self):
        print("Time:", time.localtime(time.time()))
        self.load_training_data()
        for i in range(self.iterations):
            print("\nIteration:", i+1, "| Generating Training Data.")
            self.data_history += self.create_training_set()

            # trim data if needed
            overflow = len(self.data_history) - self.train_on_n_recent_games
            if overflow > 0:
                del self.data_history[:overflow]

            self.save_training_data()
            torch.save(self.latest_net.state_dict(), "./models/tmp.pt")
            self.retrain_network(self.data_history)
            self.evaluate_network()
            print("Time:", time.localtime(time.time()))
        print("The model upgraded its best net", self.n_new_nets, "/", self.iterations, "times!")

    def create_training_set(self):
        training_data = []
        self._white_wins = 0
        self._black_wins = 0
        self.turns = 0
        for game in range(self.n_games_to_sim):
            print("Simulating:", game+1, "/", self.n_games_to_sim, end="\r")
            training_data += self.sim_one_game()
        print()
        print("White Wins:", self._white_wins, "Black Wins:", self._black_wins)
        print("Average Turns:", self.turns/self.n_games_to_sim)
        return training_data

    def retrain_network(self, dataset):
        """AGZ samples minibatch of 2048 positions from last 500,000 games, 1000 times."""
        device = torch.device("cuda:0")
        print(device)
        self.latest_net.to(device)
        report_every_n = 20
        for i in range(self.train_loops):
            running_loss, running_val_loss, running_pol_loss = 0.0, 0.0, 0.0
            batch = random.sample(dataset, 64)
            states, v_targs, p_targs = zip(*batch)
            states = torch.FloatTensor(states)
            states = states.to(device)
            v_targs, p_targs = torch.tensor(v_targs), torch.tensor(p_targs)
            v_targs = v_targs.unsqueeze(1)
            v_targs, p_targs = v_targs.to(device), p_targs.to(device)
            # zero the parameter gradients
            self.optimizer.zero_grad()

            # forward + backward + optimize
            v, p = self.latest_net(states)
            val_loss = self.val_criterion(v, v_targs)
            pol_loss = -torch.nn.functional.log_softmax(p, 1) * p_targs
            pol_loss = pol_loss.sum(dim=1).mean()
            if val_loss.item() < .00001:
                print("val_loss too low:", val_loss)
            if pol_loss.item() < .00001:
                print("pol_loss too low:", pol_loss)
            loss = torch.add(val_loss, pol_loss)
            loss.backward()
            self.optimizer.step()

            # print statistics
            running_loss += loss.item()
            running_val_loss += val_loss.item()
            running_pol_loss += pol_loss.item()
            if i % report_every_n == report_every_n-1:    
                print('[%d] running loss: %.3f val loss: %.3f pol loss: %.3f' %
                      (i+1, running_loss, running_val_loss, running_pol_loss))
                running_loss, running_val_loss, running_pol_loss = 0.0, 0.0, 0.0
        print('Finished Training')

    def evaluate_network(self):
        wins = 0
        loses = 0
        for i in range(self.eval_games):
            winner = Playground(self.latest_net, self.best_net).play_game()
            if winner == 1:
                wins += 1
            else:
                loses += 1
            games_played = i + 1
            win_percent = wins / self.eval_games
            if win_percent > .55:
                break
            elif (loses / self.eval_games) >= .45:
                break
        if win_percent > .55:
            print("New Best Net! Win Rate:", wins, "/", games_played, ",", wins/games_played*100, "%")
            self.n_new_nets += 1
            torch.save(self.latest_net.state_dict(), "./models/tmp.pt")
            torch.save(self.latest_net.state_dict(), "./models/best.pt")
            self.best_net = self.latest_net.__class__()
            self.best_net.load_state_dict(torch.load("./models/best.pt"))
            self.best_net.eval()
        else:
            print("Failed to overcome best net. Win Rate:", wins, "/", games_played, ",", wins/games_played*100, "%")
            self.latest_net = ResNetwork()
            self.latest_net.load_state_dict(torch.load("./models/tmp.pt"))
            self.latest_net.eval()
            self.latest_net.to(torch.device("cuda:0"))
            self.optimizer = torch.optim.Adam(self.latest_net.parameters(), lr=0.001)
            print("Reverting to previous net")

    def sim_one_game(self):
        self.game = Game()
        self.mcts = MCTS(self.game, self.best_net)
        train_data = []
        episodeStep = 0
        while True:
            episodeStep += 1
            temp = int(episodeStep < self.temp_threshold)
            state = self.game.state_as_tensor(self.game.board, self.game.player_in_turn)
            p = self.mcts.get_action_probs(state, temp)
            action = np.random.choice(len(p), p=p)
            train_data.append([state.tolist(), self.game.player_in_turn, p])
            t = torch.zeros(2*3*8*8)
            t[action] = 1
            move = self.game.move_flat_tensor_to_move(t)
            if move[1] in self.game.valid_moves(move[0]):
                self.game.move(move[0], move[1])
            else:
                raise Exception("Tried to make an invalid move.")
            if self.game.winner is not None:
                if self.game.winner == Player.WHITE: self._white_wins += 1
                elif self.game.winner == Player.BLACK: self._black_wins += 1
                self.turns += episodeStep
                return [(s, torch.tensor(-(-1)**(player==self.game.winner),dtype=torch.float), p) for s, player, p in train_data]

    def save_training_data(self):
        folder = "./data"
        if not os.path.exists(folder):
            os.makedirs(folder)
        filename = os.path.join(folder, "best.examples")
        with open(filename, "wb+") as f:
            Pickler(f).dump(self.data_history)
        print("Training data saved")
        f.closed

    def load_training_data(self):
        try:
            path = "./data/best.examples"
            with open(path, "rb") as f:
                self.data_history = Unpickler(f).load()
            f.closed
            print("Successfully loaded training data!", len(self.data_history), "instances loaded")
        except Exception as ee:
            print("Failed to load training data!")
            print(ee)
            if int(input("Enter 1 to contine without data. (old data may be overwritten): ")) != 1:
               exit() 
            

def test_simed_game_is_valid():
    data = Trainer(Game(), NN()).sim_one_game()
    g = Game()
    i = 0
    for _, _, action in data:
        fromloc, toloc = g._movenum_to_move(action)
        if toloc not in g.valid_moves(fromloc):
            board = g.state_as_tensor(g.board, g.player_in_turn)
            raise Exception("The simulated game is invalid.\nFailed at turn " + str(i) +
                            "\nMove from: " + str(fromloc) + " to: " + str(toloc) +
                            "\nPlayer in turn is: " + str(g.player_in_turn) +
                            " Here is the board:\n" + str(board))
        else:
            g.move(fromloc, toloc)
        i += 1
    return True

def test_simed_game_has_multiple_winners():
    trainer = Trainer(Game(), NN())
    white = 0
    black = 0
    for i in range(5):
        data = trainer.sim_one_game()
        winner = data[-1][0][2][0][0]
        print(winner)
        if winner == 0:
            white += 1
        elif winner == 1:
            black += 1
    #for state, player, _ in data:
        #print(player)
    #print(data)
    print("White Wins:", white, "Black Wins:", black)


def main():
    game = Game()
    net = ResNetwork()
    net.load_state_dict(torch.load("./models/best.pt"))
    net.eval()
    t = Trainer(game, net)
    t.train()
    #print(t.sim_one_game())
    #test_simed_game_is_valid()
    #test_simed_game_has_multiple_winners()

if __name__ == '__main__':
    main()
