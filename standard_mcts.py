# MCTS with no deep Learning

import random
import math

class MCTS:
    def __init__(self, game, color):
        self.game = game
        self.root = game.board
        self.c = 1  # tunable bias parameter for exploration

    def selection():
        pass

    def expansion():
        pass

    def simulation():
        pass

    def backup():
        pass

    def _upper_confidence_bound(node):
        ucb = (node.value() + self.c *
               math.sqrt(math.log(node.parent.visit_count()) /
                         node.visit_count()))
        return ucb

class Node:
    def __init__(self, parent):
        self.parent = parent
        self.visit_count

    def value(self):
        pass

    def visit_count(self):
        return self.visit_count
