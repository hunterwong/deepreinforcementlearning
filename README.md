# DeepReinforcementLearning

In order to train an agent, run trainer.py.
This program will load data and save the data that is generated. The best model so far is also loaded an overwritten as it improves.

If you want to play against the agent you can run play_ai_.py. Located in Games/breakthrough.

The playground.py file contains the playground class which allows the user to have two networks compete using an MCTS.

breakthrough_mcts2.py contains the Monte Carlo Search Tree. The MCTS class went through a few iterations but has ultimately settled like this.
breakthroughNN.py contains a NN class with a more simple neural network architecture that was able to learn and improve. The ResNetwork class is the newer architecture that was recently implemented. This architecture follows the architecture used by AlphaGo Zero but with fewer layers. The ResNetwork will hopefully be better than the original.

The sim_game.py file is an older file that was used to play games using earlier and simpler of models or random agents.
winner_model.py and value_model.py were used for experimentation purposes and not necessary for the current system.
