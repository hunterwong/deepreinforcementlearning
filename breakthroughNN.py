# breakthroughNN.py

import ast

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.utils.data
import numpy as np

class NN(nn.Module):
    def __init__(self):
        super(NN, self).__init__()
        self.input_shape = (3, 8, 8)
        NUM_FILTERS = 512
        self.NUM_FILTERS = NUM_FILTERS

        self.conv1 = nn.Sequential(
            nn.Conv2d(self.input_shape[0], NUM_FILTERS, kernel_size=3),
            nn.BatchNorm2d(NUM_FILTERS),
            nn.ReLU()
        )
        self.conv2 = nn.Sequential(
            nn.Conv2d(NUM_FILTERS, NUM_FILTERS, kernel_size=3),
            nn.BatchNorm2d(NUM_FILTERS),
            nn.ReLU()
        )
        self.conv3 = nn.Sequential(
            nn.Conv2d(NUM_FILTERS, NUM_FILTERS, kernel_size=3),
            nn.BatchNorm2d(NUM_FILTERS),
            nn.ReLU()
        )
        self.conv4 = nn.Sequential(
            nn.Conv2d(NUM_FILTERS, NUM_FILTERS, kernel_size=3, padding=1),
            nn.BatchNorm2d(NUM_FILTERS),
            nn.ReLU()
        )
        self.lin1 = nn.Sequential(
            nn.Linear(NUM_FILTERS*2*2, 512),
            nn.Dropout(.4),
            nn.ReLU()
        )
        self.lin2 = nn.Sequential(
            nn.Linear(512, 256),
            nn.Dropout(.4),
            nn.ReLU()
        )
        self.val_out = nn.Sequential(
            nn.Linear(256, 1),
            nn.Tanh()
        )
        # Can select illegal moves. Need to set values to zero asking game for valid moves
        self.policy_out = nn.Sequential(
            nn.Linear(256, 2*3*8*8)
        )

    def forward(self, x):
        batch_size = x.size()[0]
        #x = x.unsqueeze(0)
        #print(x.size())
        #x = x.view(-1, 3, self.input_shape[1], self.input_shape[2])
        x = self.conv1(x)
        x = self.conv2(x)
        x = self.conv3(x)
        x = self.conv4(x)
        #x = torch.flatten(x)
        #x = x.view(-1, self.NUM_FILTERS*2*2)
        #print(x.size())
        #print(x)
        x = self.lin1(x.view(batch_size, -1))
        x = self.lin2(x)
        val = self.val_out(x)
        policy = self.policy_out(x)
        return val, policy


class ResNetwork(nn.Module):
    def __init__(self):
        super(ResNetwork, self).__init__()
        self.input_shape = (3, 8, 8)
        NUM_FILTERS = 256
        self.NUM_FILTERS = NUM_FILTERS

        self.conv_in = nn.Sequential(
            nn.Conv2d(self.input_shape[0], NUM_FILTERS, 3, padding=1),
            nn.BatchNorm2d(NUM_FILTERS),
            nn.ReLU()
        )

        self.res_block1 = nn.Sequential(
            nn.Conv2d(NUM_FILTERS, NUM_FILTERS, 3, padding=1),
            nn.BatchNorm2d(NUM_FILTERS),
            nn.ReLU(),
            nn.Conv2d(NUM_FILTERS, NUM_FILTERS, 3, padding=1),
            nn.BatchNorm2d(NUM_FILTERS),
        )
        self.res_block2 = nn.Sequential(
            nn.Conv2d(NUM_FILTERS, NUM_FILTERS, 3, padding=1),
            nn.BatchNorm2d(NUM_FILTERS),
            nn.ReLU(),
            nn.Conv2d(NUM_FILTERS, NUM_FILTERS, 3, padding=1),
            nn.BatchNorm2d(NUM_FILTERS),
        )
        self.res_block3 = nn.Sequential(
            nn.Conv2d(NUM_FILTERS, NUM_FILTERS, 3, padding=1),
            nn.BatchNorm2d(NUM_FILTERS),
            nn.ReLU(),
            nn.Conv2d(NUM_FILTERS, NUM_FILTERS, 3, padding=1),
            nn.BatchNorm2d(NUM_FILTERS),
        )
        self.res_block4 = nn.Sequential(
            nn.Conv2d(NUM_FILTERS, NUM_FILTERS, 3, padding=1),
            nn.BatchNorm2d(NUM_FILTERS),
            nn.ReLU(),
            nn.Conv2d(NUM_FILTERS, NUM_FILTERS, 3, padding=1),
            nn.BatchNorm2d(NUM_FILTERS),
        )
        self.res_block5 = nn.Sequential(
            nn.Conv2d(NUM_FILTERS, NUM_FILTERS, 3, padding=1),
            nn.BatchNorm2d(NUM_FILTERS),
            nn.ReLU(),
            nn.Conv2d(NUM_FILTERS, NUM_FILTERS, 3, padding=1),
            nn.BatchNorm2d(NUM_FILTERS),
        )

        # value head
        self.val_conv = nn.Sequential(
            nn.Conv2d(NUM_FILTERS, 1, 1),
            nn.BatchNorm2d(1),
            nn.ReLU(),
        )
        self.val_lin = nn.Sequential(
            nn.Linear(8*8, 256),
            nn.ReLU(),
            nn.Linear(256, 1),
            nn.Tanh()
        )

        # policy head
        self.pol_conv = nn.Sequential(
            nn.Conv2d(NUM_FILTERS, 2, 1),
            nn.BatchNorm2d(2),
            nn.ReLU()
        )
        self.pol_lin = nn.Sequential(
            nn.Linear(8*8*2, 2*3*8*8)
        )

    def forward(self, states):
        batch_size = states.size()[0]
        x = self.conv_in(states)
        x = F.relu(x + self.res_block1(x))
        x = F.relu(x + self.res_block2(x))
        x = F.relu(x + self.res_block3(x))
        x = F.relu(x + self.res_block4(x))
        x = F.relu(x + self.res_block5(x))
        val = self.val_conv(x)
        val = self.val_lin(val.view(batch_size, -1))
        pol = self.pol_conv(x)
        pol = self.pol_lin(pol.view(batch_size, -1))
        return val, pol
        
        
