# MCTS

import random
import math

import torch
import numpy as np

from Games.breakthrough.breakthrough.model import Game, Player


class MCTS:
    def __init__(self, game, net):
        self.game = game
        #self.color = color
        self.net = net
        self.cpuct = .5  # tunable bias parameter for exploration
        self.nsims = 25

        self.Ns = {}  # visit count for each state
        self.Nsa = {}  # the count for each edge {(s,a): count}
        self.policies = {}  # {s: policy}
        # self.total_values = {}  # total value
        self.Qsa = {}  # {(s,a): v} average value of everything below

        self.Vs = {} # stores valid moves for each state

    def get_action_probs(self, state, temp=1):
        """Performs MCTS simulations to choose the next move.
        State is the tensor representation."""
        init_state = state
        for i in range(self.nsims):
            self.game.set_state(state)
            self.search(state)
        self.game.set_state(init_state)
        counts = [self.Nsa[(state,a)] if (state, a) in self.Nsa else 0 for a in range(2*3*8*8)]
        counts = torch.tensor(counts)
        l = [s for s, a in self.Nsa if torch.all(torch.eq(s,state))]

        if temp==0:
            bestA = np.argmax(counts)
            probs = [0]*len(counts)
            probs[bestA]=1
            return probs

        counts = [x**(1./temp) for x in counts]
        total = float(sum(counts))
        probs = [float(x)/total for x in counts]
        return probs

    def search(self, state):
        """State is the tensor representing a state."""
        # if node is a winning state return -1 or 1
        if self.game.winner == Player.WHITE:
            if state[2][0][0] == 0: # black did the search and white won
                return -1
            elif state[2][0][0] == 1: # white did the search and white won
                return 1
        elif self.game.winner == Player.BLACK:
            if state[2][0][0] == 0: # black did the search and black won
                return 1
            if state[2][0][0] == 1: # white did the search and black won
                return -1

        if state not in self.policies:
            device = torch.device("cpu")
            self.net.to(device)
            # state is a leaf node
            value, policy = self.net(state.unsqueeze(0))
            #policy = policy.to(torch.device("cpu"))
            #print(state)
            valids = self.game.all_valid_moves_as_flat_rep(state)
            policy = policy * valids
            policy = self._renormalize_policy(policy)
            if policy is None:  # all valid moves were 0
                # NB! All valid moves may be masked if either your NNet architecture is insufficient or you've get overfitting or something else.
                # If you have got dozens or hundreds of these messages you should pay attention to your NNet and/or training process.
                print("All valid moves were masked, do workaround.")
                policy = valids / torch.sum(valids)  # equal chance for all valid moves
            self.policies[state] = policy
            self.Vs[state] = valids
            self.Ns[state] = 0
            return -value

        best_action = self._choose_best_action(state)
        move = self.game.move_flat_tensor_to_move(best_action)
        self.game.move(move[0], move[1])
        next_state = self.game.state_as_tensor(self.game.board, self.game.player_in_turn)
        #next_state = self.game.result_of_move(move[0], move[1])
        #next_player = self.game.player_in_turn
        #next_state = self.game.state_as_tensor(next_state, next_player)

        value = self.search(next_state)
        self._update_tree(value, state, best_action)
        return -value

    def _update_tree(self, v, s, a):
        self.Ns[s] += 1
        if (s,a) in self.Qsa:
            self.Qsa[(s,a)] = (self.Nsa[(s,a)]*self.Qsa[(s,a)] + v)/(self.Nsa[(s,a)]+1)
            a = int(torch.argmax(a))
            self.Nsa[(s,a)] += 1
        else:
            self.Qsa[(s,a)] = v
            a = int(torch.argmax(a))
            self.Nsa[(s,a)] = 1

    def _choose_best_action(self, state):
        """Chooses action that maximizes the upper confidence bound or q+u."""
        valids = self.Vs[state]
        cur_best = -float('inf')
        for action in range(2*3*8*8):
            if valids[action]:
                if (state, action) in self.Qsa:
                    print("MADEIT -- _choose_best_action in MCST -- Probs never here unless repeat state")
                    u = ( self.cpuct * self.policies[state][action] *
                         math.sqrt(self.Ns[state])/(1+self.Nsa[(s,a)]) )
                    ucb = self.Qsa[(s,a)] + u
                else:
                    # assume Q = 0. Paper does not specify
                    ucb = self.cpuct * self.policies[state][action] * math.sqrt(self.Ns[state] + 1e-8) # withough 1e-8 white always wins???
                if ucb > cur_best:
                    cur_best = ucb
                    best_action = action
        action_tensor = torch.zeros(2*3*8*8)
        action_tensor[best_action] = 1
        return action_tensor

    def _renormalize_policy(self, policy):
        policy_sum = torch.sum(policy)
        if policy_sum > 0:
            #policy /= policy_sum
            return policy / policy_sum
        else:
            return None
