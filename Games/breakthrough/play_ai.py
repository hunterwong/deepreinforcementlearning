import tkinter as tk
import os,sys,inspect

import torch
import numpy as np


from breakthrough.model import Game, Player
from breakthrough.gui import View
from breakthrough.presenter import Presenter

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
grandparentdir = os.path.dirname(parentdir)
sys.path.insert(0, grandparentdir)
from breakthrough_mcts2 import MCTS
from breakthroughNN import NN, ResNetwork


class Agent:
    def __init__(self, color, nn, mcts_sims):
        self.color = color
        self.game = Game()
        self.mcts = MCTS(self.game, nn)
        self.mcts.sims = mcts_sims

    def get_move(self, state):
        state = self.game.state_as_tensor(state, self.color)
        action = np.argmax(self.mcts.get_action_probs(state, temp=0))
        #print(self.mcts.Qsa[(state,action)])
        t = torch.zeros(2*3*8*8)
        t[action] = 1
        move = self.game.move_flat_tensor_to_move(t)
        return move

def main():
    app = tk.Tk()
    app.title("Breakthrough")
    net = ResNetwork()
    try:
        net.load_state_dict(torch.load("../../models/best.pt"))
    except RuntimeError:
        print("reloading torch with cpu")
        net.load_state_dict(torch.load("../../models/best.pt", map_location=torch.device("cpu")))

    net.eval()
    color = Player.BLACK
    agent = Agent(color, net, 15)
    Presenter(Game(), View(app, ai=agent))
    app.mainloop()

if __name__ == "__main__":
    main()
