import tkinter as tk

from breakthrough.gui import View
from breakthrough.model import Player
from breakthrough.model import Location as rc


info = """
Manual Tests (Breakthrough View Test)
1. Window pops up
2. Window contains Board Widget
3. Board displays white marker at (0,1) and black at (1,3)
4. View displays that it's white's move
5. View displays an info message (dismiss it)
6. View displays a file open dialog (cancel it)
7. View displays a file save dialog suggesting foo.data (cancel it)
8. View has menu items for File: Load, Save and Game: New Game, Quit
9a Pop up indicates a move was made
9. Selecting menu items produces a message indicating they were selected
10. Selecting Quit quits the test
"""


class MockEventHandler:

    def __init__(self, view):
        self.view = view

    def handle_open_event(self):
        self.view.flash("Load")

    def handle_quit_event(self):
        self.view.shutdown()

    def handle_save_event(self):
        self.view.flash("Save")

    def handle_move_event(self, src, dest):
        self.view.flash("move"+str(src)+str(dest))

    def handle_new_game_event(self):
        self.view.flash("New Game")


def test_view():
    print(info)
    app = tk.Tk()
    app.title("Breakthrough View Test")
    v = View(app)
    v.set_board({rc(0, 1): Player.WHITE, rc(1, 3): Player.BLACK})
    v.set_player_in_turn(Player.WHITE)
    v.flash("Important Message")
    v.get_file_to_open()
    v.get_file_to_save_in("foo.data")
    v.set_event_handler(MockEventHandler(v))
    v.board.generate_move_event(rc(0, 0), rc(0, 1))
    app.mainloop()
