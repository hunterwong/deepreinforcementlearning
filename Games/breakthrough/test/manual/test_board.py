import tkinter as tk
from breakthrough.gui import Board
from breakthrough.model import Location as rc
from breakthrough.model import Player

info = """
Tests (Manual Test: Board widget)
  1. White marker at (0,1) and black at (1,3)
  2. Clicking anything but white marker has no effect
  3. Click on white marker shows 3 move targets (green circles)
  4. White marker is draggable
  5. Dropping white marker anywhere except on green target snaps back
  6. Dropping white marker on green target prints "move made" to destination
"""


def mock_validator(loc):
    if loc == rc(0, 1):
        return [rc(1, 0), rc(1, 1), rc(1, 2)]
    else:
        return []


def mock_observer(loc_from, loc_to):
    print("Move made from:", loc_from, "to", loc_to)


def test_board_widget():
    print(info)
    root = tk.Tk()
    root.title("Manual Test: Board widget")
    b = Board(root)
    b.grid()
    b.set_move_validator(mock_validator)
    b.subscribe_move_notifications(mock_observer)
    b.display({rc(0, 1): Player.WHITE, (1, 3): Player.BLACK})
    root.mainloop()
