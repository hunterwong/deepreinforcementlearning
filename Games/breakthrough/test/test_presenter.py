# Hunter Wong implemented newgame


import unittest
from unittest.mock import create_autospec

from breakthrough.presenter import Presenter
from breakthrough.model import Location as rc
from breakthrough.model import Player, Game, LoadError, SaveError
from breakthrough.gui import View


class TestPresenter(unittest.TestCase):

    def setUp(self):
        self.view = create_autospec(View)  # Mock()
        self.model = create_autospec(Game)  # Mock()
        self.initial_board = {
                    rc(0, 0): Player.WHITE, rc(0, 1): Player.WHITE,
                    rc(2, 0): Player.BLACK, rc(2, 1): Player.BLACK,
                    }
        self.model.initial_board = self.initial_board
        self.model.board = self.model.initial_board
        self.model.player_in_turn = Player.WHITE
        self.model.winner = None
        self.p = Presenter(self.model, self.view)
        self.changed_board = {
                    rc(0, 0): Player.WHITE, rc(1, 1): Player.WHITE,
                    rc(2, 0): Player.BLACK, rc(2, 1): Player.BLACK,
                    }

    def test_displays_new_game_when_app_opens(self):
        self.view.set_player_in_turn.assert_called_with(Player.WHITE)
        self.view.set_board.assert_called_with(self.initial_board)
        self.view.set_winner.assert_called_with(None)

    def test_sets_self_as_event_handler(self):
        self.view.set_event_handler.assert_called_with(self.p)

    def test_sets_move_validator_when_app_opens(self):
        self.view.set_move_validator.assert_called_once_with(
            self.model.valid_moves)

    def test_move_updates_board_in_model_and_view(self):
        self.model.board = self.changed_board
        self.p.handle_move_event(rc(0, 1), rc(1, 1))
        self.model.move.assert_called_once_with(rc(0, 1), rc(1, 1))
        self.view.set_board.assert_called_with(self.changed_board)

    def test_move_updates_player_in_turn(self):
        self.model.player_in_turn = Player.BLACK
        self.p.handle_move_event(rc(0, 0), rc(0, 1))
        self.view.set_player_in_turn.assert_called_with(Player.BLACK)

    def test_move_updates_winner(self):
        self.model.winner = Player.WHITE
        self.p.handle_move_event(rc(0, 0), rc(0, 1))
        self.view.set_winner.assert_called_with(Player.WHITE)

    def test_open_valid_filename_loads_model_and_updates_view(self):
        self.view.get_file_to_open.return_value = "test_file.data"
        self.model.board = self.changed_board
        self.p.handle_open_event()
        self.model.load_game.assert_called_with("test_file.data")
        self.view.set_board.assert_called_with(self.changed_board)

    def test_open_file_gets_canceled_leaves_model_unchanged(self):
        self.view.get_file_to_open.return_value = None
        self.p.handle_open_event()
        self.model.load_game.assert_not_called()

    def test_open_bad_file_notifies_user(self):
        self.view.get_file_to_open.return_value = "test_file.data"
        self.model.load_game.side_effect = LoadError("LOAD ERROR")
        self.p.handle_open_event()
        self.view.flash.assert_called_with("LOAD ERROR")

    def test_save_game_successfully(self):
        self.model.filename = "test_file.data"
        self.view.get_file_to_save_in.return_value = "filename_chosen.data"
        self.p.handle_save_event()
        self.view.get_file_to_save_in.assert_called_with("test_file.data")
        self.model.save_game.assert_called_with("filename_chosen.data")
        self.view.flash.assert_called_with("Saved to filename_chosen.data.")

    def test_save_game_cancels_successfully(self):
        self.model.filename = "test_file.data"
        self.view.get_file_to_save_in.return_value = None
        self.p.handle_save_event()
        self.model.save_game.assert_not_called()

    def test_unsucessful_save_notifies_user(self):
        self.model.filename = ""
        self.view.get_file_to_save_in.return_value("fname_chosen.data")
        self.model.save_game.side_effect = SaveError("SAVE ERROR")
        self.p.handle_save_event()
        self.view.flash.assert_called_with("SAVE ERROR")

    def test_handle_quit_event(self):
        self.p.handle_quit_event()
        self.view.shutdown.assert_called_with()

    def test_handle_new_game_event(self):
        self.p.handle_new_game_event()
        self.p.model.newgame.assert_called_with()
