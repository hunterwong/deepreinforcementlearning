# Hunter Wong implemented newgame


import unittest
import shutil
import os
import torch

from breakthrough.model import Game, Player, Location as rc


class TestGame(unittest.TestCase):

    def setUp(self):
        self.g = Game()

    def check_row_filled_with(self, row, player):
        for c in range(8):
            loc = rc(row, c)
            self.assertEqual(self.g.board[loc], player)

    def test_start_row_0_all_black(self):
        self.check_row_filled_with(0, Player.BLACK)

    def test_start_row_1_all_black(self):
        self.check_row_filled_with(1, Player.BLACK)

    def test_start_row_7_all_white(self):
        self.check_row_filled_with(7, Player.WHITE)

    def test_start_row_6_all_white(self):
        self.check_row_filled_with(6, Player.WHITE)

    def test_white_moves_first(self):
        self.assertEqual(self.g.player_in_turn, Player.WHITE)

    def test_start_with_32_pieces(self):
        self.assertEqual(len(self.g.board), 32)

    def test_start_with_no_winner(self):
        self.assertIsNone(self.g.winner)

    def test_move_alternates_to_black(self):
        self.g.move(rc(6, 0), rc(5, 0))
        self.assertEqual(self.g.player_in_turn, Player.BLACK)

    def test_move_alternates_to_white(self):
        self.g.move(rc(6, 0), rc(5, 0))
        self.g.move(rc(1, 0), rc(2, 0))
        self.assertEqual(self.g.player_in_turn, Player.WHITE)

    def test_move_changes_board(self):
        fromloc = rc(6, 0)
        toloc = rc(5, 0)
        g = self.g
        g.move(fromloc, toloc)
        self.assertEqual(g.board[toloc], Player.WHITE)
        self.assertNotIn(fromloc, g.board)

    def test_different_move_changes_board(self):
        fromloc = rc(1, 0)
        toloc = rc(2, 0)
        g = self.g
        g.move(fromloc, toloc)
        self.assertEqual(g.board[toloc], Player.BLACK)
        self.assertNotIn(fromloc, g.board)

    def test_white_winning_move(self):
        self.g.move(rc(6, 0), rc(5, 0))
        self.assertIsNone(self.g.winner)
        self.g.move(rc(5, 0), rc(0, 0))
        self.assertEqual(self.g.winner, Player.WHITE)

    def test_black_winning_move(self):
        self.g.move(rc(1, 0), rc(2, 0))
        self.assertIsNone(self.g.winner)
        self.g.move(rc(2, 0), rc(7, 0))
        self.assertEqual(self.g.winner, Player.BLACK)

    def test_no_valid_moves_for_empty_location(self):
        dests = self.g.valid_moves(rc(3, 3))
        self.assertEqual(dests, [])

    def test_white_can_move_to_empty_locations(self):
        dests = self.g.valid_moves(rc(6, 3))
        self.assertEqual(sorted(dests),
                         [rc(5, 2), rc(5, 3), rc(5, 4)])

    def test_black_can_move_to_empty_locations(self):
        self.g.move(rc(6, 3), rc(5, 2))
        dests = self.g.valid_moves(rc(1, 4))
        self.assertEqual(sorted(dests),
                         [rc(2, 3), rc(2, 4), rc(2, 5)])

    def test_no_valid_moves_for_out_of_turn_pieces(self):
        # try move for BLACK
        dests = self.g.valid_moves(rc(2, 4))
        self.assertEqual(dests, [])
        self.g.move(rc(6, 3), rc(5, 2))
        self.assertEqual(self.g.valid_moves(rc(5, 2)), [])

    def test_no_valid_move_after_win(self):
        self.g.move(rc(6, 0), rc(0, 0))
        self.assertEqual(self.g.valid_moves(rc(1, 4)), [])

    def test_forward_move_not_valid_if_blocked(self):
        # blocked by own piece
        self.assertEqual(self.g.valid_moves(rc(7, 0)), [])
        self.g.move(rc(6, 0), rc(2, 0))
        # blocked by opponent's piece
        self.assertEqual(self.g.valid_moves(rc(1, 0)), [rc(2, 1)])

    def test_diagonal_capture_allowed(self):
        self.g.move(rc(6, 0), rc(2, 0))
        dests = self.g.valid_moves(rc(1, 1))
        self.assertIn(rc(2, 0), dests)

    def test_save_and_restore(self):
        # instant win for white
        g = self.g
        self.assertEqual(g.filename, None)
        g.move(rc(6, 0), rc(0, 0))
        g.save_game("test.tmp")
        self.assertEqual(g.filename, "test.tmp")
        g2 = Game()
        shutil.copyfile("test.tmp", "test_copy.tmp")
        g2.load_game("test_copy.tmp")
        self.assertEqual(g.board, g2.board)
        self.assertEqual(g.winner, g2.winner)
        self.assertEqual(g.player_in_turn, g2.player_in_turn)
        self.assertEqual(g2.filename, "test_copy.tmp")
        os.remove("test.tmp")
        os.remove("test_copy.tmp")

    # TODO tests for canceled and "broken" saves and restores

    def test_new_game_resets_game_to_initial_state(self):
        self.g.player_in_turn = Player.BLACK
        self.g.board = {
                    rc(0, 0): Player.WHITE, rc(0, 1): Player.WHITE,
                    rc(2, 0): Player.BLACK, rc(2, 1): Player.BLACK,
                    }
        self.g.winner = Player.BLACK
        self.g.newgame()
        self.assertEqual(self.g.board, self.g.initial_board)
        self.assertEqual(self.g.player_in_turn, Player.WHITE)
        self.assertEqual(self.g.winner, None)

    def test_init_game_state_as_tensor(self):
        tensor = self.g.state_as_tensor(self.g.board, Player.WHITE)
        #print(tensor)
        correct_tensor = torch.tensor([[[0, 0, 0, 0, 0, 0, 0, 0],
                                        [0, 0, 0, 0, 0, 0, 0, 0],
                                        [0, 0, 0, 0, 0, 0, 0, 0],
                                        [0, 0, 0, 0, 0, 0, 0, 0],
                                        [0, 0, 0, 0, 0, 0, 0, 0],
                                        [0, 0, 0, 0, 0, 0, 0, 0],
                                        [1, 1, 1, 1, 1, 1, 1, 1],
                                        [1, 1, 1, 1, 1, 1, 1, 1]],

                                       [[1, 1, 1, 1, 1, 1, 1, 1],
                                        [1, 1, 1, 1, 1, 1, 1, 1],
                                        [0, 0, 0, 0, 0, 0, 0, 0],
                                        [0, 0, 0, 0, 0, 0, 0, 0],
                                        [0, 0, 0, 0, 0, 0, 0, 0],
                                        [0, 0, 0, 0, 0, 0, 0, 0],
                                        [0, 0, 0, 0, 0, 0, 0, 0],
                                        [0, 0, 0, 0, 0, 0, 0, 0]],

                                        [[0, 0, 0, 0, 0, 0, 0, 0],
                                         [0, 0, 0, 0, 0, 0, 0, 0],
                                         [0, 0, 0, 0, 0, 0, 0, 0],
                                         [0, 0, 0, 0, 0, 0, 0, 0],
                                         [0, 0, 0, 0, 0, 0, 0, 0],
                                         [0, 0, 0, 0, 0, 0, 0, 0],
                                         [0, 0, 0, 0, 0, 0, 0, 0],
                                         [0, 0, 0, 0, 0, 0, 0, 0]]],
                                     dtype=torch.float)
        self.assertTrue(torch.all(torch.eq(tensor, correct_tensor)))

    def test_initial_board_tensor_to_dict(self):
        tensor = self.g.state_as_tensor(self.g.board, Player.WHITE)
        state_dict = self.g.state_tensor_to_dict(tensor)
        self.assertEqual(state_dict, self.g.board)

    def test_move_as_flat_rep_2_3_diagonal_right(self):
        fromloc = rc(2,3)
        toloc = rc(1,4)
        correct_tens = torch.zeros(2,3,8,8, dtype=torch.float)
        correct_tens[0][2][2][3] = 1
        correct_tens = correct_tens.flatten()
        rep = self.g.move_as_flat_rep(fromloc, toloc, Player.WHITE)
        #print(rep)
        #print(correct_tens)
        self.assertTrue(torch.all(torch.eq(rep, correct_tens)))

    def test_all_valid_moves_flat_rep_from_inital_state(self):
        state_tensor = self.g.state_as_tensor(self.g.board, self.g.player_in_turn)
        mvs = self.g.all_valid_moves_as_flat_rep(state_tensor)
        correct = torch.zeros(2,3,8,8)
        for dim in range(3):
            for col in range(8):
                correct[0][dim][6][col] = 1
        correct[0][0][6][0], correct[0][2][6][7] = 0, 0 # since left and right peice dont have diagnol move
        correct = correct.flatten()
        self.assertTrue(torch.all(torch.eq(mvs, correct)))

    def test_all_valid_moves_flat_rep_from_inital_state_other_player(self):
        state_tensor = self.g.state_as_tensor(self.g.board, Player.BLACK)
        mvs = self.g.all_valid_moves_as_flat_rep(state_tensor)
        correct = torch.zeros(2,3,8,8)
        for dim in range(3):
            for col in range(8):
                correct[1][dim][1][col] = 1
        correct[1][0][1][0], correct[1][2][1][7] = 0, 0 # since left and right peice dont have diagnol move
        correct = correct.flatten()
        self.assertTrue(torch.all(torch.eq(mvs, correct)))

    def test_tensor_move_2_3_to_move_loc_white(self):
        correct_fromloc = rc(2,3)
        correct_toloc = rc(1,4)
        rep = self.g.move_as_flat_rep(correct_fromloc, correct_toloc, Player.WHITE)
        fromloc, toloc = self.g.move_flat_tensor_to_move(rep)
        self.assertEqual((fromloc,toloc),(correct_fromloc, correct_toloc))

    def test_tensor_move_2_3_to_move_loc_black(self):
        # what black move looks like in tensor since board is rotated
        correct_fromloc = rc(2,3)
        correct_toloc = rc(3,4)
        rep = self.g.move_as_flat_rep(correct_fromloc, correct_toloc, Player.BLACK)
        fromloc, toloc = self.g.move_flat_tensor_to_move(rep)
        self.assertEqual((fromloc,toloc), (correct_fromloc,correct_toloc))
