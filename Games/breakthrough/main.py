import tkinter as tk

from breakthrough.model import Game
from breakthrough.gui import View
from breakthrough.presenter import Presenter


app = tk.Tk()
app.title("Breakthrough")
Presenter(Game(), View(app))
app.mainloop()
