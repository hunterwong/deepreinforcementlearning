"""Breakthrough Game"""


import torch

from typing import NamedTuple
from enum import Enum


class Player(Enum):
    WHITE = "white"
    BLACK = "black"


class Location(NamedTuple):
    row: int
    col: int


class LoadError(Exception):
    pass


class SaveError(Exception):
    pass


class Game:

    def __init__(self):
        """Game represents the state of a breakthrough game played
        on an 8x8 chessboard.

        public (read-only) variables:
        board: a dictionary of location:color values for pieces on the board
        filename: file that game was last loaded from/saved to (initially None)
        player_in_turn: the player currently to play
        winner: the winner of the game (initially None)
        """
        self.initial_board = {}
        for col in range(8):
            self.initial_board[Location(0, col)] = Player.BLACK
            self.initial_board[Location(1, col)] = Player.BLACK
            self.initial_board[Location(6, col)] = Player.WHITE
            self.initial_board[Location(7, col)] = Player.WHITE
        self.board = self.initial_board.copy()
        self.player_in_turn = Player.WHITE
        self.winner = None
        self.filename = None

    def newgame(self):
        self.board = self.initial_board.copy()
        self.player_in_turn = Player.WHITE
        self.winner = None

    def save_game(self, fname):
        """Saves current state of the game to file named fname.
        Note: raises SaveError if unsuccessful
        """
        self.filename = fname
        with open(fname, "w") as outfile:
            print(self.player_in_turn.value, file=outfile)
            if self.winner is None:
                print(file=outfile)
            else:
                print(self.winner.value, file=outfile)
            for loc, player in self.board.items():
                print(loc.row, loc.col, player.value, file=outfile)

    def load_game(self, fname):
        """Load a previously saved game from fname.
        Note: raises LoadError if unsuccessful"""
        with open(fname, "r") as infile:
            self.player_in_turn = self._parse_player(infile.readline())
            self.winner = self._parse_player_or_none(infile.readline())
            self.board = {}
            for line in infile:
                loc, player = self._parse_loc_player(line)
                self.board[loc] = player
        self.filename = fname

    def _parse_player(self, txt):
        txt = txt.strip()
        if txt == Player.WHITE.value:
            return Player.WHITE
        elif txt == Player.BLACK.value:
            return Player.BLACK
        else:
            raise LoadError("Bad value for player "+str(txt))

    def _parse_player_or_none(self, txt):
        txt = txt.strip()
        if txt:
            return self._parse_player(txt)
        else:
            return None

    def _parse_loc_player(self, txt):
        txt = txt.strip()
        r, c, p = txt.split()
        loc = Location(int(r), int(c))
        player = self._parse_player(p)
        return loc, player

    def move(self, fromloc, toloc):
        """Moves piece located at fromloc to toloc.
        Note: the move is assumed OK. Rules of play are not enforced.
        """
        if self.player_in_turn == Player.BLACK:
            self.player_in_turn = Player.WHITE
        else:
            self.player_in_turn = Player.BLACK
        self.board[toloc] = self.board[fromloc]
        del self.board[fromloc]
        self.winner = self._home_row(toloc)

    def valid_moves(self, loc):
        """Returns list of valid destinations locations for piece at startloc.
        Note: Enforces rules of the game. If there is no valid move, returns []
        """
        if not self._is_valid_move_start(loc):
            dests = []
        else:
            row_inc = 1 if self.player_in_turn == Player.BLACK else -1
            dests = [Location(loc.row + row_inc, c)
                     for c in range(loc.col-1, loc.col + 2)]
            dests = [d for d in dests if self._is_valid_landing(loc, d)]
        return dests

    def _is_valid_move_start(self, loc):
        return (self.winner is None
                and loc in self.board
                and self.board[loc] == self.player_in_turn)

    def _is_valid_landing(self, src, dest):
        return (self._is_on_board(dest)
                and (dest not in self.board
                     or self._is_valid_capture(src, dest)))

    def _is_on_board(self, loc):
        return 0 <= loc.row <= 7 and 0 <= loc.col <= 7

    def _is_valid_capture(self, src, dest):
        return (src.col != dest.col
                and self.board[src] != self.board[dest])

    def _home_row(self, loc):
        if loc.row == 0:
            return Player.WHITE
        elif loc.row == 7:
            return Player.BLACK
        else:
            return None

    def set_state(self, state_tensor):
        self.board = self.state_tensor_to_dict(state_tensor)
        if state_tensor[2][0][0] == 0:
            self.player_in_turn = Player.WHITE
        else:
            self.player_in_turn = Player.BLACK
        self._check_winner()

    def _check_winner(self):
        for (row,col), player in self.board.items():
            if row == 0 and player == Player.WHITE:
                winner = Player.WHITE
            elif row == 7 and player == Player.BLACK:
                winner = Player.BLACK
            else:
                winner = None
            self.winner = winner

    def state_as_tensor(self, board_state, color_in_turn):
        tensor = torch.zeros([3,8,8], dtype=torch.float)
        """
        white_locs = [loc for loc, player in board_state.items() if player == Player.WHITE]
        black_locs = [loc for loc, player in board_state.items() if player == Player.BLACK]
        for loc in white_locs:
            tensor[0][loc.row][loc.col] = 1
        for loc in black_locs:
            tensor[1][loc.row][loc.col] = 1
        """
        for loc, player in board_state.items():
            if player == Player.WHITE:
                tensor[0][loc.row][loc.col] = 1
            elif player == Player.BLACK:
                tensor[1][loc.row][loc.col] = 1
        if color_in_turn == Player.BLACK:
            for i in range(8):
                for j in range(8):
                    tensor[2][i][j] = 1
        return tensor

    def state_tensor_to_dict(self, state_tensor):
        if state_tensor[2][0][0] == 0:
            player_turn = Player.WHITE
        else:
            player_turn = Player.BLACK
        state = {}
        for dim in range(2):
            for row in range(8):
                for col in range(8):
                    if state_tensor[dim][row][col] == 1:
                        if dim == 0:
                            state[Location(row, col)] = Player.WHITE
                        elif dim == 1:
                            state[Location(row, col)] = Player.BLACK
        return state

    def result_of_move(self, fromloc, toloc):
        """Moves piece located at fromloc to toloc.
        Note: the move is assumed OK. Rules of play are not enforced.
        """
        fake_board = self.board.copy()
        fake_board[toloc] = fake_board[fromloc]
        del fake_board[fromloc]
        return fake_board

    def all_valid_moves(self, state_tensor):
        if state_tensor[2][0][0] == 0:
            player_turn = Player.WHITE
        else:
            player_turn = Player.BLACK
        state_dict = self.state_tensor_to_dict(state_tensor)
        moves = []
        #print(state_tensor)
        #print(self.board)
        for loc, player in state_dict.items():
            if player == player_turn:
                dests = self._valid_moves_any_piece(loc, player)
                for dest in dests:
                    moves.append((loc, dest))
        return moves

    def _valid_moves_any_piece(self, loc, player):
        """loc of piece to be moved and player that owns the piece"""
        row_inc = 1 if player == Player.BLACK else -1
        dests = [Location(loc.row + row_inc, c)
                 for c in range(loc.col-1, loc.col + 2)]
        dests = [d for d in dests if self._is_valid_landing(loc, d)]
        return dests


    def all_valid_moves_as_flat_rep(self, state_tensor):
        """Returns a flat tensor filled with 0s for invalid moves and 1 for valid"""
        player_turn = int(state_tensor[2][0][0])  # 0 is white 1 is black
        valid_mvs = self.all_valid_moves(state_tensor)
        flat_valid_mvs = torch.zeros(2,3,8,8, dtype=torch.float)
        mv_indicies_for_tensor = []
        for fromloc, toloc in valid_mvs:
            dir = self._tensor_loc_for_move(fromloc, toloc)
            flat_valid_mvs[player_turn][dir][fromloc.row][fromloc.col] = 1
        flat_valid_mvs = flat_valid_mvs.flatten()
        return flat_valid_mvs

    def move_as_tensor(self, fromloc, toloc, player_turn):
        """Move must be legal."""
        if player_turn == Player.WHITE:
            player_turn = 0
        else:
            player_turn = 1
        assert(abs(fromloc.row - toloc.row) == 1)
        assert(abs(fromloc.col - toloc.col) <= 1)
        rep = torch.zeros(2,3,8,8, dtype=torch.float)
        dir = self._tensor_loc_for_move(fromloc, toloc)
        rep[player_turn][dir][fromloc.row][fromloc.col] = 1
        return rep

    def move_as_flat_rep(self, fromloc, toloc, player_turn):
        rep = self.move_as_tensor(fromloc, toloc, player_turn).flatten()
        return rep

    def _tensor_loc_for_move(self, fromloc, toloc):
        assert(abs(fromloc.row - toloc.row) == 1)
        assert(abs(fromloc.col - toloc.col) <= 1)
        col_dir = toloc.col - fromloc.col + 1  # 0 is left, 1 is forward, 2 is right
        return col_dir

    def move_flat_tensor_to_move(self, move_tensor):
        # 0-63 is diaganol left 64-127 is forward 127-191 is diaganol right
        movelst = move_tensor.tolist()
        moveloc = movelst.index(1)
        if moveloc <= 191:
            player = -1  # Player.WHITE negative moves up the board
        else:
            player = 1  # Player.BLACK move down to board
            moveloc -= 192  # so same check for black and white

        if moveloc <= 63:
            row = moveloc // 8
            dir = -1
        elif 64 <= moveloc <= 127:
            row = (moveloc-64) // 8
            dir = 0
        elif 128 <= moveloc <= 191:
            row = (moveloc-128) // 8
            dir = 1
        col = moveloc % 8
        fromloc = Location(row,col)
        toloc = Location(row+player, col+dir)
        return fromloc, toloc

    def movenum_to_move(self, num):
        move_tensor = torch.zeros(2,3,8,8, dtype=torch.float).flatten()
        move_tensor[num] = 1
        fromloc, toloc = self.move_flat_tensor_to_move(move_tensor)
        return fromloc, toloc
