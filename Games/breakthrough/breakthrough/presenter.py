# Hunter Wong implemented newgame


from breakthrough.model import LoadError, SaveError


class Presenter:

    def __init__(self, model, view):
        self.model = model
        self.view = view
        self.view.set_move_validator(self.model.valid_moves)
        self.view.set_event_handler(self)
        self._update_game_view()

    def _update_game_view(self):
        self.view.set_board(self.model.board)
        self.view.set_player_in_turn(self.model.player_in_turn)
        self.view.set_winner(self.model.winner)

    def handle_move_event(self, fromloc, toloc):
        if not self.model.winner:
            self.model.move(fromloc, toloc)
            self._update_game_view()

    def handle_new_game_event(self):
        self.model.newgame()
        self._update_game_view()

    def handle_open_event(self):
        fname = self.view.get_file_to_open()
        if fname:
            try:
                self.model.load_game(fname)
                self._update_game_view()
            except LoadError as e:
                self.view.flash(str(e))

    def handle_save_event(self):
        suggested_fname = self.model.filename or ""
        actual_fname = self.view.get_file_to_save_in(suggested_fname)
        if actual_fname:
            try:
                self.model.save_game(actual_fname)
                self.view.flash("Saved to "+actual_fname+".")
            except SaveError as e:
                self.view.flash(str(e))

    def handle_quit_event(self):
        self.view.shutdown()
