import tkinter as tk
from tkinter import ttk
from tkinter import messagebox as mbox
from tkinter import filedialog as fd
from breakthrough.model import Player, Location

_token_colors = {Player.WHITE: "ivory", Player.BLACK: "black"}


class Board(ttk.Frame):
    """Breakthrough game board widget"""

    def __init__(self, parent, ai=None):
        self.scale = 1
        super().__init__(parent)
        self.parent = parent
        self.ai = ai
        self.canvas = tk.Canvas(width=400*self.scale, height=400*self.scale, highlightthickness=0)
        self.canvas.grid(sticky=tk.N)
        self._draw_checkerboard("red4", "gray75")
        # draginfo is a triple (item_id, x, y) of token being dragged
        self._draginfo = None
        # moveinfo is a pair (start_location, legal_destinations_list) of move
        #    in progress (token being dragged).
        self._moveinfo = None
        # move_validator function: f((r, c)) --> [legal (r,c) destinations]
        self._move_validator = None
        # move_observers are called when a successful move completes
        self._move_observers = []
        # visual state of the board`
        self._board_state = None
        #self.vsai = None
        self.canvas.tag_bind("token", "<ButtonPress-1>", self._start_drag)
        self.canvas.tag_bind("token", "<ButtonRelease-1>", self._end_drag)
        self.canvas.tag_bind("token", "<B1-Motion>", self._drag)

    def display(self, board_state):
        """Dislay board with token locations given by board_state dictionary
        e.g. board_state: {(0, 1): "white", (1, 3): "black"}
        """
        self.canvas.delete("token")
        for loc, player in board_state.items():
            color = _token_colors[player]
            self._draw_token(loc, color)
        self._board_state = board_state

    def set_move_validator(self, validator):
        self._move_validator = validator

    def subscribe_move_notifications(self, callback):
        self._move_observers.append(callback)

    def generate_move_event(self, src, dest):
        for obs in self._move_observers:
            obs(Location(*src), Location(*dest))

    def _draw_checkerboard(self, color1, color2):
        for row in range(8):
            for col in range(8):
                fill = color1 if (row+col) % 2*self.scale == 0 else color2
                x, y = self._to_canvas((row, col))
                offset = 25*self.scale
                self.canvas.create_rectangle(x-offset, y-offset, x+offset, y+offset, fill=fill)

    def _draw_token(self, coord, color):
        (x, y) = self._to_canvas(coord)
        radius = 40*self.scale/2
        self.canvas.create_oval(x-radius, y-radius, x+radius, y+radius,
                                outline=color, fill=color, tags="token")

    def _start_drag(self, event):
        xy = (event.x, event.y)
        item = self.canvas.find_closest(*xy)[0]
        # make sure thing to drag is a token (don't grab square below)
        if item not in self.canvas.find_withtag("token"):
            return
        rowcol = self._from_canvas(xy)
        dests = self._move_validator(Location(*rowcol))
        if dests:
            self._moveinfo = (rowcol, dests)
            self._draginfo = (item, *xy)
            self._set_marks(dests)
            self.canvas.tag_raise(item)

    def _end_drag(self, event):
        '''End drag of an object'''
        # reset the drag information
        if self._draginfo:
            self._clear_marks()
            self._draginfo = None
            dest = self._from_canvas((event.x, event.y))
            start_loc, valid_dests = self._moveinfo
            if dest in valid_dests:
                self.generate_move_event(start_loc, dest)
                if self.ai:
                    move = self.ai.get_move(self._board_state)
                    self.generate_move_event(move[0], move[1])
            else:
                self._restore_state()

    def _restore_state(self):
        self.display(self._board_state)

    def _drag(self, event):
        '''Handle dragging of an object'''
        # compute how much the mouse has moved
        if self._draginfo:
            item, drag_x, drag_y = self._draginfo
            delta_x = event.x - drag_x
            delta_y = event.y - drag_y
            self.canvas.move(item, delta_x, delta_y)
            self._draginfo = (item, event.x, event.y)

    def _to_canvas(self, loc):
        row, col = loc
        x = 50*self.scale*col + 25*self.scale
        y = 50*self.scale*row + 25*self.scale
        return (x, y)

    def _from_canvas(self, xy):
        x, y = xy
        col = int(round((x-25*self.scale)/(50*self.scale)))
        row = int(round((y-25*self.scale)/(50*self.scale)))
        return (row, col)

    def _set_marks(self, locs):
        r = 8
        for loc in locs:
            x, y = self._to_canvas(loc)
            self.canvas.create_oval(x-r, y-r, x+r, y+r,
                                    fill="lightgreen", tags="marker")

    def _clear_marks(self):
        self.canvas.delete("marker")


class View:

    def __init__(self, root, ai=None):
        self.root = root
        root.columnconfigure(0, weight=1)
        root.rowconfigure(0, weight=1)
        mainframe = ttk.Frame(root)
        mainframe.grid(column=0, row=0, sticky=(tk.N, tk.W, tk.E, tk.S))
        self.style = ttk.Style()
        self.style.configure('msg.TLabel', font=("", 18, "bold"))
        self.board = Board(mainframe, ai=ai)
        self.board.grid(column=0, row=1, sticky=tk.N)
        self.board.rowconfigure(0, weight=1)
        self.status = ttk.Label(mainframe, style="msg.TLabel", anchor=tk.S)
        self.status.grid(column=0, row=0, sticky=tk.S)
        mainframe.columnconfigure(0, weight=1)
        mainframe.rowconfigure(0, weight=1)
        mainframe.rowconfigure(1, weight=0)
        self._create_menus(root)
        self.event_handler = None

    def _create_menus(self, root):
        root.option_add('*tearOff', tk.FALSE)
        menubar = tk.Menu(root)
        root.configure(menu=menubar)
        menu_file = tk.Menu(menubar)
        menu_game = tk.Menu(menubar)
        menubar.add_cascade(menu=menu_file, label='File')
        menubar.add_cascade(menu=menu_game, label='Game')
        menu_file.add_command(label='Open...',
                command=lambda: self.event_handler.handle_open_event())
        menu_file.add_command(label='Save...',
                command=lambda: self.event_handler.handle_save_event())
        menu_game.add_command(label="New Game",
                command=lambda: self.event_handler.handle_new_game_event())
        menu_game.add_command(label='Quit',
                command=lambda: self.event_handler.handle_quit_event())

    def set_move_validator(self, validator_fn):
        """Sets validator function used to ensure that gui does allow
        user to generate an illegal moveself.
        Note: validator_fn has signature: startloc --> [legal destinations]
        """
        self.board.set_move_validator(validator_fn)

    def set_event_handler(self, handler):
        """Sets handler to handle all (model-level) events."""
        self.event_handler = handler
        self.board.subscribe_move_notifications(handler.handle_move_event)

    def set_board(self, board_dict):
        """Updates view to display board configuration in board_dict
        Note: board_dict is a dictionary of loc:Player pairs for all
        pieces on the board.
        """
        self.board.display(board_dict)

    def set_player_in_turn(self, player):
        """Updates view to show that it's player's turn."""
        color = _token_colors[player].title()
        self.status.config(text=color+"'s Move")

    def set_winner(self, player):
        """Updates view to show that player is the winner.
        Note: None is used for game still in progress (no winner yet)
        """
        if player:
            color = _token_colors[player].title()
            self.status.config(text=color+" Wins!")

    def get_file_to_open(self):
        """Initiates a modal interaction to get a file name from user."""
        return fd.askopenfilename()

    def get_file_to_save_in(self, suggested_fname):
        """Initiates a modal interation to get a filename from user."""
        return fd.asksaveasfilename(initialfile=suggested_fname)

    def flash(self, msg: str):
        """Notifies user of msg"""
        mbox.showinfo("Information", msg)

    def shutdown(self):
        """Quits the application."""
        self.root.destroy()
