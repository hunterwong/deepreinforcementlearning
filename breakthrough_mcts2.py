# breakthrough_mcts
#


import math
import numpy as np
import torch
from Games.breakthrough.breakthrough.model import Game, Player

EPS = 1e-8

class MCTS():
    """
    This class handles the MCTS tree.
    """

    def __init__(self, game, nnet):
        self.game = game
        self.nnet = nnet
        try:
            self.nnet.to("cuda:0")
        except AssertionError:
            pass
        self.sims = 150
        self.cpuct = 4
        self.Qsa = {}       # stores Q values for s,a (as defined in the paper)
        self.Nsa = {}       # stores #times edge s,a was visited
        self.Ns = {}        # stores #times board s was visited
        self.Ps = {}        # stores initial policy (returned by neural net)

        self.Es = {}        # stores game.getGameEnded ended for board s
        self.Vs = {}        # stores game.getValidMoves for board s

    def get_action_probs(self, s, temp=1):
        """
        Returns:
            probs: a policy vector where the probability of the ith action is
                   proportional to Nsa[(s,a)]**(1./temp)
        """
        for i in range(self.sims):
            self.game.set_state(s)
            self.search(s)
        self.game.set_state(s)

        counts = [self.Nsa[(s,a)] if (s,a) in self.Nsa else 0 for a in range(2*3*8*8)]
        if temp==0:
            bestA = np.argmax(counts)
            probs = [0]*len(counts)
            probs[bestA]=1
            return probs

        counts = [x**(1./temp) for x in counts]
        total = sum(counts)
        probs = [float(x)/total for x in counts]
        return probs


    def search(self, s):
        """
        This function performs one iteration of MCTS. It is recursively called
        till a leaf node is found. The action chosen at each node is one that
        has the maximum upper confidence bound as in the paper.
        Once a leaf node is found, the neural network is called to return an
        initial policy P and a value v for the state. This value is propogated
        up the search path. In case the leaf node is a terminal state, the
        outcome is propogated up the search path. The values of Ns, Nsa, Qsa are
        updated.
        NOTE: the return values are the negative of the value of the current
        state. This is done since v is in [-1,1] and if v is the value of a
        state for the current player, then its value is -v for the other player.
        Returns:
            v: the negative of the value of the current state
        """
        # if node is a winning state return -1 or 1        
        if self.game.winner:
            if self.game.winner.value == Player.WHITE.value:
                #print("found a white winning state in mcts")
                if s[2][0][0] == 0: # black did the search and white won
                    return -1
                elif s[2][0][0] == 1: # white did the search and white won
                    return 1
            elif self.game.winner.value == Player.BLACK.value:
                #print("found a black winning state in mcts")
                if s[2][0][0] == 0: # black did the search and black won
                    return 1
                if s[2][0][0] == 1: # white did the search and black won
                    return -1
            else:
                print("MCTS Winner value not working")
                prtin(self.game.winner.value)

        if s not in self.Ps:
            # leaf node
            with torch.no_grad():
                device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
                s_gpu = s.to(device)
                v, p = self.nnet(s_gpu.unsqueeze(0))
                v, p = v.to("cpu"), p.to("cpu")
                v = v[0]
                self.Ps[s] = torch.nn.functional.softmax(p[0], dim=0)
                #print(v)
                valids = self.game.all_valid_moves_as_flat_rep(s)
                self.Ps[s] = self.Ps[s]*valids      # masking invalid moves
                #print(self.Ps[s])
                #self.Ps[s][self.Ps[s]<0] = 0  # sets negatives to 0
                
                sum_Ps_s = torch.sum(self.Ps[s])
                if sum_Ps_s > 0:
                    self.Ps[s] /= sum_Ps_s    # renormalize
                    #print()
                else:
                    # if all valid moves were masked make all valid moves equally probable

                    # NB! All valid moves may be masked if either your NNet architecture is insufficient or you've get overfitting or something else.
                    # If you have got dozens or hundreds of these messages you should pay attention to your NNet and/or training process.
                    print("All valid moves were masked, do workaround.")
                    #print(sum_Ps_s)
                    self.Ps[s] = self.Ps[s] + valids
                    self.Ps[s] /= torch.sum(self.Ps[s])
            self.Vs[s] = valids
            self.Ns[s] = 0
            return -v

        valids = self.Vs[s]
        cur_best = -float('inf')
        best_act = -1

        # pick the action with the highest upper confidence bound
        for a in range(2*3*8*8):
            if valids[a]:
                if (s,a) in self.Qsa:
                    u = self.Qsa[(s,a)] + self.cpuct*self.Ps[s][a]*math.sqrt(self.Ns[s])/(1+self.Nsa[(s,a)])
                else:
                    u = self.cpuct*self.Ps[s][a]*math.sqrt(self.Ns[s] + EPS)     # Q = 0 ?

                if u > cur_best:
                    cur_best = u
                    best_act = a

        a = best_act
        mv = self.game.movenum_to_move(a)
        self.game.move(mv[0], mv[1])
        next_s = self.game.state_as_tensor(self.game.board, self.game.player_in_turn)

        v = self.search(next_s)

        if (s,a) in self.Qsa:
            self.Qsa[(s,a)] = (self.Nsa[(s,a)]*self.Qsa[(s,a)] + v)/(self.Nsa[(s,a)]+1)
            self.Nsa[(s,a)] += 1

        else:
            self.Qsa[(s,a)] = v
            self.Nsa[(s,a)] = 1

        self.Ns[s] += 1
        return -v
