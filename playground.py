# playground.py

import random

import torch
import numpy as np

from Games.breakthrough.breakthrough.model import Game, Player
from breakthrough_mcts2 import MCTS
from breakthroughNN import NN, ResNetwork


class Playground:
    """Class for playing two networks against eachother using MCTS"""
    def __init__(self, nn1, nn2):
        """nn1 is White, nn2 is Black"""
        device = torch.device("cpu")
        nn1.to(device)
        nn2.to(device)
        self.game = Game()
        self.p1 = MCTS(Game(), nn1)
        self.p2 = MCTS(Game(), nn2)
        players = [self.p1, self.p2]
        random.shuffle(players)
        self.white = players[0]
        self.black = players[1]

    def play_game(self):
        """Returns either 1 or 2, the number of the player that won."""
        alternator = 1  # 1 is white -1 is black
        while self.game.winner is None:
            state = self.game.state_as_tensor(self.game.board, self.game.player_in_turn)
            if alternator == 1:
                action = np.argmax(self.white.get_action_probs(state))
            elif alternator == -1:
                action = np.argmax(self.black.get_action_probs(state))
            alternator = -alternator
            t = torch.zeros(2*3*8*8)
            t[action] = 1
            move = self.game.move_flat_tensor_to_move(t)
            if move[1] in self.game.valid_moves(move[0]):
                self.game.move(move[0], move[1])
            else:
                raise Exception("Tried to make an invalid move.")
        if self.game.winner == Player.WHITE:
            if self.white == self.p1: return 1
            else: return 2
        if self.game.winner == Player.BLACK:
            if self.black == self.p1: return 1
            else: return 2

    def set_mcts_sims(self, sims):
        self.p1.sims = sims
        self.p2.sims = sims

def run_best_vs_random():
    print("Running the best net vs a random net")
    best_net = NN()
    best_net.load_state_dict(torch.load("./models/best.pt"))
    rand_net = NN()
    best_wins = 0
    rand_wins = 0
    for i in range(20):
        winner = Playground(best_net, rand_net).play_game()
        if winner == 1:
            best_wins += 1
        elif winner == 2:
            rand_wins += 1
    print("best wins:", best_wins, "rand wins:", rand_wins)

def battle():
    best_net = ResNetwork()
    #best_net.load_state_dict(torch.load("./models/best.pt"))
    best_net.eval()
    prev_net = ResNetwork()
    #prev_net.load_state_dict(torch.load("./models/best.pt"))
    prev_net.eval()
    best_wins = 0
    prev_wins = 0
    for i in range(20):
        pg = Playground(best_net, prev_net)
        #pg.set_mcts_sims(15)
        pg.p1.sims = 2
        pg.p2.sims = 2
        winner = pg.play_game()
        if winner == 1:
            best_wins += 1
        elif winner == 2:
            prev_wins += 1
    print("#1 Wins:", best_wins, "#2 Wins:", prev_wins)
    

def main():
    #run_best_vs_random()
    battle()
    
if __name__ == '__main__':
    main()
